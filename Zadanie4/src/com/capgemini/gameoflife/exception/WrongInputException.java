package com.capgemini.gameoflife.exception;

public class WrongInputException extends Exception {

	public WrongInputException() {
		super("Wprowadzono wspolrzedna przekraczajaca rozmiar planszy z gra.");
	}
}
