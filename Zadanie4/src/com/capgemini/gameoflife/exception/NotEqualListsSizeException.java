package com.capgemini.gameoflife.exception;

public class NotEqualListsSizeException extends Exception {

	public NotEqualListsSizeException() {
		super("Wprowadzono liczby o roznej ilosci elemenow.");
	}
}
