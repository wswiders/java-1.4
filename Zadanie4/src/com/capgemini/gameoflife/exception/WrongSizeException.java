package com.capgemini.gameoflife.exception;

public class WrongSizeException extends Exception {

	public WrongSizeException() {
		super("Wprowadzono nieprawidlowy rozmiar planszy do gry.");
	}
}
