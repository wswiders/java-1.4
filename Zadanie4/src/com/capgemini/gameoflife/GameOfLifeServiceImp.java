package com.capgemini.gameoflife;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.capgemini.gameoflife.cell.Cell;
import com.capgemini.gameoflife.exception.NotEqualListsSizeException;
import com.capgemini.gameoflife.exception.NumberOfCellsException;
import com.capgemini.gameoflife.exception.WrongInputException;
import com.capgemini.gameoflife.exception.WrongSizeException;

public class GameOfLifeServiceImp implements GameOfLifeService {

	private HashMap<Integer, Cell> cellsInGame = new HashMap<>();
	private final int maxBoardSize = 1000;
	private int boardWidth, boardHeight;

	private boolean findCellStatus(int hashCode) {
		if(cellsInGame.containsKey(hashCode)){
			return cellsInGame.get(hashCode).getCellStatus();
		}
		return false;
	}

	@Override
	public void gameInit(int sizeOfBoard) throws WrongSizeException {
		if (sizeOfBoard > 0 && sizeOfBoard <= maxBoardSize) {
			for (int y = 1; y <= sizeOfBoard; y++) {
				for (int x = 1; x <= sizeOfBoard; x++) {
					cellsInGame.put(x + (int) Math.pow(maxBoardSize, 2) * y, new Cell(x, y));
				}
			}
			this.boardHeight = sizeOfBoard;
			this.boardWidth = sizeOfBoard;
		} else {
			throw new WrongSizeException();
		}
	}

	@Override
	public void gameInit(int widthOfBoard, int hightOfBoard) throws WrongSizeException {
		if (widthOfBoard > 0 && widthOfBoard <= maxBoardSize && hightOfBoard > 0 && hightOfBoard <= maxBoardSize) {
			for (int y = 1; y <= hightOfBoard; y++) {
				for (int x = 1; x <= widthOfBoard; x++) {
					cellsInGame.put(x + (int) Math.pow(maxBoardSize, 2) * y, new Cell(x, y));
				}
			}
			this.boardHeight = hightOfBoard;
			this.boardWidth = widthOfBoard;
		} else {
			throw new WrongSizeException();
		}
	}

	public HashMap<Integer, Cell> getCellsInGame() {
		return cellsInGame;
	}

	@Override
	public void killOrRebornCells() {
		setNumberOfNeighboursInCell();
		for (int hashCode : cellsInGame.keySet()) {
			Cell cell = cellsInGame.get(hashCode);
			if ((cell.getCellStatus() && cell.getNumberOfNeighbours() < 2)
					|| (cell.getCellStatus() && cell.getNumberOfNeighbours() > 3)) {
				cell.setCellStatus(false);
			}
			if (!cell.getCellStatus() && cell.getNumberOfNeighbours() == 3) {
				cell.setCellStatus(true);
			}
		}
	}

	public String printGameBoard() {
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i <= boardHeight; i++) {
			for (int j = 1; j <= boardWidth; j++) {
				int hashCode = j + (int) Math.pow(maxBoardSize, 2) * i;
				if (cellsInGame.get(hashCode).getCellStatus()) {
					sb.append("#  ");
				} else {
					sb.append(".  ");
				}
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	private void setNumberOfNeighboursInCell() {
		for (int hashCode : cellsInGame.keySet()) {
			List<Integer> listOfNeighbours = cellsInGame.get(hashCode).getListOfHashCodesOfNeighbours();
			int numberOfNeigh = 0;
			for (int neighbourHashCode : listOfNeighbours) {
				if (findCellStatus(neighbourHashCode)) {
					numberOfNeigh++;
				}
			}
			cellsInGame.get(hashCode).setNumberOfNeighbours(numberOfNeigh);
		}
	}

	@Override
	public void setAliveCellFromList(List<Integer> positionX, List<Integer> positionY)
			throws NotEqualListsSizeException, NumberOfCellsException, WrongInputException {
		int sizeOfGameBoard = this.cellsInGame.size();

		if (positionX.size() != positionY.size()) {
			throw new NotEqualListsSizeException();
		}
		if (Collections.max(positionX) > boardWidth || Collections.max(positionY) > boardHeight
				|| Collections.min(positionX) <= 0 || Collections.min(positionY) <= 0) {
			throw new WrongInputException();
		}

		if (!(positionX.size() > sizeOfGameBoard) && positionX.size() > 0) {
			for (int i = 0; i < positionX.size(); i++) {
				int hashCode = positionX.get(i) + (int) Math.pow(maxBoardSize, 2) * positionY.get(i);
				for (int hashCodeOfCell : cellsInGame.keySet()) {
					if (cellsInGame.get(hashCodeOfCell).getHashCode() == hashCode) {
						cellsInGame.get(hashCodeOfCell).setCellStatus(true);
					}
				}
			}
		} else {
			throw new NumberOfCellsException();
		}
	}

	@Override
	public void setRandomCellAlive(int numberOfCells) throws NumberOfCellsException {
		Random rnd = new Random();
		int sizeOfGameBoard = this.cellsInGame.size();

		if (!(numberOfCells > sizeOfGameBoard) && numberOfCells > 0) {
			while (numberOfCells != 0) {
				int x = rnd.nextInt(boardWidth);
				int y = rnd.nextInt(boardHeight);
				int hashCode = x + (int) Math.pow(maxBoardSize, 2) * y;
				if (x >= 1 && y >= 1 && !cellsInGame.get(hashCode).getCellStatus()) {
					cellsInGame.get(hashCode).setCellStatus(true);
					numberOfCells--;
				}
			}
		} else {
			throw new NumberOfCellsException();
		}
	}
}