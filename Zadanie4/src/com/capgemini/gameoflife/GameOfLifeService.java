package com.capgemini.gameoflife;

import java.util.List;

import com.capgemini.gameoflife.exception.NotEqualListsSizeException;
import com.capgemini.gameoflife.exception.NumberOfCellsException;
import com.capgemini.gameoflife.exception.WrongInputException;
import com.capgemini.gameoflife.exception.WrongSizeException;

public interface GameOfLifeService {

	/**
	 * Initialize a game square board with @Cell on it
	 * @param sizeOfBoard size of square board
	 * @throws WrongSizeException 
	 */
	public void gameInit(int sizeOfBoard) throws WrongSizeException;
	
	/**
	 * Initialize a game rectangle board with @Cell on it
	 * @param widthOfBoard width of game board 
	 * @param hightOfBoard height of game board
	 * @throws WrongSizeException 
	 */
	public void gameInit(int widthOfBoard, int hightOfBoard) throws WrongSizeException;
	
	/**
	 * Insert random alive cell to game board
	 * @param numberOfCells number of alive cells to insert
	 * @throws NumberOfCellsException
	 */
	public void setRandomCellAlive(int numberOfCells) throws NumberOfCellsException;
	
	/**
	 * Insert alive cells from a list
	 * @param positionX list of X coordinates of alive cells
	 * @param positionY	list of Y coordinates of alive cells
	 * @throws NotEqualListsSizeException
	 * @throws NumberOfCellsException 
	 * @throws WrongInputException 
	 */
	void setAliveCellFromList(List<Integer> positionX, List<Integer> positionY) throws NotEqualListsSizeException, NumberOfCellsException, WrongInputException;
	
	/**
	 * set @Cell parameters to true or false (cell is alive or dead)
	 */
	void killOrRebornCells();	
}

