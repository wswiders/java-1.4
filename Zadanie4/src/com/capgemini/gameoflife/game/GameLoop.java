package com.capgemini.gameoflife.game;

import com.capgemini.gameoflife.GameOfLifeServiceImp;

public class GameLoop {

	public static void main(String[] args) throws Exception {
		GameOfLifeServiceImp gameOfLife = new GameOfLifeServiceImp();

		gameOfLife.gameInit(20);
		gameOfLife.setRandomCellAlive(200);
		System.out.println(gameOfLife.printGameBoard());

		for (int i = 0; i < 100; i++) {
			Thread.sleep(1000);
			gameOfLife.killOrRebornCells();
			System.out.println(gameOfLife.printGameBoard());
		}
	}
}
