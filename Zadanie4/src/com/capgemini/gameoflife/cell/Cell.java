package com.capgemini.gameoflife.cell;

import java.util.ArrayList;
import java.util.List;

public class Cell {

	private int hashCode;
	private int x, y;
	private final int Multi = (int)Math.pow(1000, 2);
	private List<Integer> listOfHashCodesOfNeighbours = new ArrayList<>();
	private int numberOfNeighbours;
	private boolean cellStatus = false;

	public Cell(int x, int y) {
		this.x = x;
		this.y = y;
		this.setHashCode();
		this.setListOfHashCodesOfNeighbours();
	}

	public int getHashCode() {
		return hashCode;
	}

	public List<Integer> getListOfHashCodesOfNeighbours() {
		return listOfHashCodesOfNeighbours;
	}

	public int getNumberOfNeighbours() {
		return numberOfNeighbours;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean getCellStatus() {
		return cellStatus;
	}

	public void setCellStatus(boolean cellStatus) {
		this.cellStatus = cellStatus;
	}

	private void setHashCode() {
		this.hashCode = x + Multi * y;
	}

	private void setListOfHashCodesOfNeighbours() {
		this.listOfHashCodesOfNeighbours.add((x - 1) + Multi * (y + 1));
		this.listOfHashCodesOfNeighbours.add(x + Multi * (y + 1));
		this.listOfHashCodesOfNeighbours.add((x + 1) + Multi * (y + 1));
		this.listOfHashCodesOfNeighbours.add((x - 1) + Multi * y);
		this.listOfHashCodesOfNeighbours.add((x + 1) + Multi * y);
		this.listOfHashCodesOfNeighbours.add((x - 1) + Multi * (y - 1));
		this.listOfHashCodesOfNeighbours.add(x + Multi * (y - 1));
		this.listOfHashCodesOfNeighbours.add((x + 1) + Multi * (y - 1));
	}

	public void setNumberOfNeighbours(int numberOfNeighbours) {
		this.numberOfNeighbours = numberOfNeighbours;
	}
}