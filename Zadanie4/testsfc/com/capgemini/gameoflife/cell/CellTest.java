package com.capgemini.gameoflife.cell;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.gameoflife.cell.Cell;

public class CellTest {

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void shouldHashCodeEqual0WhenXEqual0AndYEqual0() {
		Cell cell = new Cell(0, 0);
		assertEquals(0, cell.getHashCode());
	}

	@Test
	public void shouldHashCodeEqual34WhenXEqual3AndYEqual5() {
		Cell cell = new Cell(3, 5);
		assertEquals(5000003, cell.getHashCode());
	}

	@Test
	public void shouldNotHashCodeEqual34WhenXEqual5AndYEqual5() {
		Cell cell = new Cell(5, 5);
		assertNotEquals(5000004, cell.getHashCode());
	}

	@Test
	public void sholudListOfNeighboursHashCodesBeCorrect() {
		List<Integer> expected = new ArrayList<>(
				Arrays.asList(6000003, 6000004, 6000005, 5000003, 5000005, 4000003, 4000004, 4000005));
		Cell cell = new Cell(4, 5);
		assertTrue(expected.equals(cell.getListOfHashCodesOfNeighbours()));
	}

	@Test
	public void sholudNotListOfNeighboursHashCodesBeCorrect() {
		List<Integer> expected = new ArrayList<>(
				Arrays.asList(6000003, 6000004, 6000005, 5000003, 5000005, 4000003, 4000005, 4000005));
		Cell cell = new Cell(4, 5);
		assertFalse(expected.equals(cell.getListOfHashCodesOfNeighbours()));
	}

}
