package com.capgemini.gameoflife;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.gameoflife.GameOfLifeServiceImp;
import com.capgemini.gameoflife.exception.NotEqualListsSizeException;
import com.capgemini.gameoflife.exception.NumberOfCellsException;
import com.capgemini.gameoflife.exception.WrongInputException;
import com.capgemini.gameoflife.exception.WrongSizeException;

public class GameOfLifeServiceImplTest {

	GameOfLifeServiceImp game;
	List<Integer> listX;
	List<Integer> listY;

	@Before
	public void setUp() throws Exception {
		game = new GameOfLifeServiceImp();
	}

	@Test
	public void shouldHashCodeBeUnique() throws WrongSizeException {
		Set<Integer> listOfHashCodes = new HashSet<>();
		game.gameInit(20);
		for (Integer hashcode : game.getCellsInGame().keySet()) {
			listOfHashCodes.add(game.getCellsInGame().get(hashcode).getHashCode());
		}
		assertEquals(20*20, listOfHashCodes.size());
	}
	
	@Test
	public void shouldSizeOfListEqual4WhenInput2() throws WrongSizeException {
		game.gameInit(2);
		assertEquals(4, game.getCellsInGame().size());
	}

	@Test
	public void shouldSizeOfListEqual8WhenInput2And4() throws WrongSizeException {
		game.gameInit(2, 4);
		assertEquals(8, game.getCellsInGame().size());
	}

	@Test(expected = WrongSizeException.class)
	public void shouldThrowWrongSizeExceptionWhenFirstGameInitInputLessEqualZero() throws WrongSizeException {
		game.gameInit(0);
	}

	@Test(expected = WrongSizeException.class)
	public void shouldThrowWrongSizeExceptionWhenSecendGameInitInputLessEqualZero() throws WrongSizeException {
		game.gameInit(0, 5);
	}

	@Test(expected = WrongSizeException.class)
	public void shouldThrowWrongSizeExceptionWhenFirstGameInitInputOver10000() throws WrongSizeException {
		game.gameInit(10001);
	}

	@Test(expected = WrongSizeException.class)
	public void shouldThrowWrongSizeExceptionWhenSecondGameInitInputOver10000() throws WrongSizeException {
		game.gameInit(10001, 43);
	}

	@Test(expected = NumberOfCellsException.class)
	public void shouldThrowNumberOfCellsExceptionWhenInputBiggerThenBoardSize() throws Exception {
		game.gameInit(10);
		game.setRandomCellAlive(101);
	}

	@Test(expected = NumberOfCellsException.class)
	public void shouldThrowNumberOfCellsExceptionWhenInputZero() throws Exception {
		game.gameInit(10);
		game.setRandomCellAlive(0);
	}

	@Test
	public void shouldInsertExact50AliveCellsWhenInputIs50() throws Exception {
		game.gameInit(10);
		game.setRandomCellAlive(50);
		int sum = 0;
		for (Integer hashCode : game.getCellsInGame().keySet()) {
			if (game.getCellsInGame().get(hashCode).getCellStatus()) {
				sum++;
			}
		}
		assertEquals(50, sum);
	}

	@Test(expected = NotEqualListsSizeException.class)
	public void shouldThrowNotEqualListsSizeExceptionWhenListHaveDifferentSize() throws Exception {
		listX = new ArrayList<>(Arrays.asList(1, 23, 4));
		listY = new ArrayList<>(Arrays.asList(1, 23, 4, 6));
		game.gameInit(10);
		game.setAliveCellFromList(listX, listY);
	}

	@Test(expected = NumberOfCellsException.class)
	public void shouldThrowNumberOfCellsExceptionWhenListsAreTooBig() throws Exception {
		listX = new ArrayList<>(Arrays.asList(1, 1, 2, 2, 1));
		listY = new ArrayList<>(Arrays.asList(1, 2, 1, 2, 1));
		game.gameInit(2);
		game.setAliveCellFromList(listX, listY);
	}

	@Test(expected = NoSuchElementException.class)
	public void shouldThrowNoSuchElementExceptionWhenListIsEmpty() throws Exception {
		listX = new ArrayList<>();
		listY = new ArrayList<>();
		game.gameInit(2);
		game.setAliveCellFromList(listX, listY);
	}

	@Test(expected = WrongInputException.class)
	public void shouldThrowWrongSizeExceptionWhenNumberInListIsTooBig() throws Exception {
		listX = new ArrayList<>(Arrays.asList(1, 1, 2));
		listY = new ArrayList<>(Arrays.asList(1, 2, 3));
		game.gameInit(2);
		game.setAliveCellFromList(listX, listY);
	}

	@Test(expected = WrongInputException.class)
	public void shouldThrowWrongSizeExceptionWhenNumberInListIsTooSmall() throws Exception {
		listX = new ArrayList<>(Arrays.asList(1, 1, 2));
		listY = new ArrayList<>(Arrays.asList(1, 2, -1));
		game.gameInit(2);
		game.setAliveCellFromList(listX, listY);
	}

	@Test
	public void shouldInsertAliveCellOnBoard() throws Exception {
		listX = new ArrayList<>(Arrays.asList(1));
		listY = new ArrayList<>(Arrays.asList(1));
		game.gameInit(2);
		game.setAliveCellFromList(listX, listY);
		assertTrue(game.getCellsInGame().get(1000001).getCellStatus());
	}

	@Test
	public void shouldInsertTwoAliveCellOnBoard() throws Exception {
		listX = new ArrayList<>(Arrays.asList(1, 2));
		listY = new ArrayList<>(Arrays.asList(1, 2));
		game.gameInit(2);
		game.setAliveCellFromList(listX, listY);
		assertTrue(game.getCellsInGame().get(1000001).getCellStatus() && game.getCellsInGame().get(2000002).getCellStatus());
	}

	@Test
	public void shouldKillCellWhenItsAliveAndHaveLessThenTwoNeighbours() throws Exception {
		listX = new ArrayList<>(Arrays.asList(1, 2));
		listY = new ArrayList<>(Arrays.asList(1, 2));
		game.gameInit(3);
		game.setAliveCellFromList(listX, listY);
		game.killOrRebornCells();
		assertFalse(game.getCellsInGame().get(2000002).getCellStatus());
	}
	
	@Test
	public void shouldKillCellWhenItsAliveAndHave0Neighbours() throws Exception {
		listX = new ArrayList<>(Arrays.asList(2));
		listY = new ArrayList<>(Arrays.asList(2));
		game.gameInit(3);
		game.setAliveCellFromList(listX, listY);
		game.killOrRebornCells();
		assertFalse(game.getCellsInGame().get(2000002).getCellStatus());
	}

	@Test
	public void shouldKillCellWhenItsAliveAndHaveMoreThen3Neighbours() throws Exception {
		listX = new ArrayList<>(Arrays.asList(1, 2, 3, 2, 1));
		listY = new ArrayList<>(Arrays.asList(1, 2, 1, 1, 2));
		game.gameInit(3);
		game.setAliveCellFromList(listX, listY);
		game.killOrRebornCells();
		assertFalse(game.getCellsInGame().get(2000002).getCellStatus());
	}

	@Test
	public void shouldStayAliveWhenItsAliveAndHave2Neighbours() throws Exception {
		listX = new ArrayList<>(Arrays.asList(1, 2, 3));
		listY = new ArrayList<>(Arrays.asList(1, 2, 1));
		game.gameInit(3);
		game.setAliveCellFromList(listX, listY);
		game.killOrRebornCells();
		assertTrue(game.getCellsInGame().get(2000002).getCellStatus());
	}

	@Test
	public void shouldStayAliveWhenItsAliveAndHave3Neighbours() throws Exception {
		listX = new ArrayList<>(Arrays.asList(1, 2, 3, 2));
		listY = new ArrayList<>(Arrays.asList(1, 2, 1, 1));
		game.gameInit(3);
		game.setAliveCellFromList(listX, listY);
		game.killOrRebornCells();
		assertTrue(game.getCellsInGame().get(2000002).getCellStatus());
	}

	@Test
	public void shouldBeRebornWhenItsDeadAndHave3Neighbours() throws Exception {
		listX = new ArrayList<>(Arrays.asList(1, 2, 3));
		listY = new ArrayList<>(Arrays.asList(1, 1, 1));
		game.gameInit(3);
		game.setAliveCellFromList(listX, listY);
		game.killOrRebornCells();
		assertTrue(game.getCellsInGame().get(2000002).getCellStatus());
	}
	
	@Test
	public void shouldBeRebornWhenItsDeadAndHaveLessThan3Neighbours() throws Exception {
		listX = new ArrayList<>(Arrays.asList(1, 2));
		listY = new ArrayList<>(Arrays.asList(1, 1));
		game.gameInit(3);
		game.setAliveCellFromList(listX, listY);
		game.killOrRebornCells();
		assertFalse(game.getCellsInGame().get(2000002).getCellStatus());
	}
	
	@Test
	public void shouldBeRebornWhenItsDeadAndHaveMoreThan3Neighbours() throws Exception {
		listX = new ArrayList<>(Arrays.asList(1, 2, 3, 1));
		listY = new ArrayList<>(Arrays.asList(1, 1, 1, 2));
		game.gameInit(3);
		game.setAliveCellFromList(listX, listY);
		game.killOrRebornCells();
		assertFalse(game.getCellsInGame().get(2000002).getCellStatus());
	}
}
